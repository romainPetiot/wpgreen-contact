<?php

add_action('wp_enqueue_scripts', 'wpgreen_contact_scripts', 999);
function wpgreen_contact_scripts()
{
	global $wp_styles;
	wp_register_script('scriptWPGreenContact', plugin_dir_url( __FILE__ ) . 'contact.js', false, false, 'all');
}

function wpgreenFormContact( $atts ) {
    if(is_admin()) return "";
	?>
        <form id="form-contact" name="form-contact" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="action" value="formContact">
            <?php wp_nonce_field('nonceformContact', 'nonceformContact'); ?>
            
            <div class="contResponse" id="contResponse">
            </div>
            <div class="formCont">
                <input type="text" value="" name="contact_name" id="contact_name" placeholder="<?php _e("Nom","wpgreen");?>*" required />
                <label for="contact_name"><?php _e("Nom","wpgreen");?>*</label>
            </div>

            <div class="formCont">
                <input type="text" value="" name="contact_company" id="contact_company" placeholder="<?php _e("Entreprise","wpgreen");?>"  />
                <label for="contact_company"><?php _e("Entreprise","wpgreen");?></label>
            </div>

            <div class="formCont"> 
                <input type="email" value="" name="contact_email" id="contact_email" placeholder="<?php _e("Email","wpgreen");?>*" required />
                <label for="contact_email"><?php _e("Email","wpgreen");?>*</label>
            </div>

            <div class="formCont">
                <input type="tel" value="" name="contact_phone" id="contact_phone" placeholder="<?php _e("Tél","wpgreen");?>" />
                <label for="contact_phone"><?php _e("Tél","wpgreen");?></label>
            </div>

            <div class="formCont">
                <textarea name="contact_comments"  id="contact_comments" placeholder="<?php _e("Commentaires","wpgreen");?>*" required ></textarea>
                <label for="contact_comments"><?php _e("Commentaires","wpgreen");?>*</label>
            </div>    

            <div class="formCheckBox">
                <input type="checkbox" name="check" id="contact_check" required />
                <label for="contact_check">
                    <?php _e("En cochant cette case et en soumettant ce formulaire, j'accepte que mes données personnelles soient utilisées pour me recontacter dans le cadre de ma demande indiquée dans ce formulaire. (Aucun autre traitement ne sera effectué avec vos informations).","wpgreen");?>
                </label>
            </div>
            <div class="formButton">
                <button class="button" type="submit" id="contact_send"><?php _e("Envoyer","wpgreen");?></button>
            </div>
        </form>
    <?php
    wp_enqueue_script('scriptWPGreenContact');
    wp_localize_script('scriptWPGreenContact', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}
add_shortcode( 'wpgreenFormContact', 'wpgreenFormContact' );