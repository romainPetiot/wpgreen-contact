document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('form-contact')) {
        document.getElementById('form-contact').addEventListener('submit', function (e) {
            e.preventDefault();
            document.getElementById('contact_send').disabled = true;
            var form = document.forms.namedItem("form-contact");
            var formData = new FormData(form);
            xhr = new XMLHttpRequest();
            xhr.open('POST', ajaxurl, true);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    document.getElementById('contact_send').disabled = false;
                    document.getElementById("contResponse").innerHTML = xhr.response;
                    window.scrollTo({
                        top: document.getElementById("content").offsetTop,
                        behavior: 'smooth',
                    });
                    document.getElementById("form-contact").classList.add("response");
                }
            }; 
            xhr.send(formData);
        });
    }
});