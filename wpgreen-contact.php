<?php
/**
 * Plugin name: WPgreenContact
 * Plugin URI: https://ihaveagreen.fr
 * Description: Formulaire de contact et export CSV
 * Author : Romain Petiot
 * Version: 1.0
 * Stable tag: 1.0
 *
 */

 //Bloquer les accès directs	
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


add_action( 'init', 'contact_custom_post_type');
function contact_custom_post_type() {
	register_post_type( 'contact', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' 			=> array(
				'name' 				=> __('Contacts', 'wpgreen'), /* This is the Title of the Group */
				'singular_name' 	=> __('Contact', 'wpgreen'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'supports' 			=> array( 'title', 'editor', 'custom-fields')
	 	) /* end of options */
    );
    
    register_taxonomy(
		'form',
        'contact',
		array(
            'label'             => __( 'Formulaire', "wpgreen" ),
            'hierarchical'      => true,
            'show_admin_column' => true
		)
	);

}

require plugin_dir_path( __FILE__ ) . '/traitementForm.php';

require plugin_dir_path( __FILE__ ) . '/export.php';

require plugin_dir_path( __FILE__ ) . '/form-contact.php';

function contact_remove( $pid ) {//delete attachment lors de la suppression du post
		global $wpdb, $post_type;
		if ( $post_type != 'contact' ) return;
		
		$attachmentid = get_post_meta( $pid, 'attachment', true);
		if(!empty($attachmentid)){
			wp_delete_attachment( $attachmentid );
			error_log(get_post_meta( $pid, 'fileAttachment', true));
			unlink(get_post_meta( $pid, 'fileAttachment', true));
		}
}
add_action( 'before_delete_post', 'contact_remove' );



