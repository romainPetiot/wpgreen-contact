<?php
// wp_dashboard_setup is the action hook
add_action('wp_dashboard_setup', 'cl_dashboard');
function cl_dashboard() {
    wp_add_dashboard_widget('cl_dashboard_widget', 'Export CSV','wpgreen_custom_dashboard_message');
}
function wpgreen_custom_dashboard_message(){
	?>
	<ul>
        <?php
        $args = array( 'posts_per_page' => 5, 'post_type' => 'contact' );
        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) :
            echo '<li><a href="'.admin_url( 'post.php?post='.$post->ID.'&action=edit').'">'.$post->post_title.'</a></li>';
        endforeach;
        ?>
    </ul>
    <p>
    <a href="?report=wpgreen_export" class="button">Export</a>
    </p>
	<?php
}



class wpgreen_export_contact
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'wpgreen_export')
        {
        	$this->wpgreen_export();
        }
    }
	public function wpgreen_export(){
		global $wpdb, $post;
        $csv_fields=array();
        /*$csv_fields[] = 'Titre';
        $csv_fields[] = 'Contenu';*/
        $csv_fields[] = "Date";
        $csv_fields[] = "Form";
        //$csv_fields[] = "Titre";
        $csv_fields[] = "Nom";
        $csv_fields[] = "Entreprise";
        $csv_fields[] = "Email";
        $csv_fields[] = "Téléphone";
        $csv_fields[] = "Commentaire";
        $csv_fields[] = "Créneau";
        $csv_fields[] = "Depuis la page";
        $csv_fields[] = "Fichier";
        $csv_fields[] = "Bien concerné";
        $csv_fields[] = "Url du bien";
        
        $output_filename = "exportContact_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,"\t" );
		
		$args = array( 'posts_per_page' => -1, 'post_type' => 'contact' );
        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) {
            setup_postdata( $post ); 
            $taxonomy_names = wp_get_post_terms( get_the_id(), 'form' );
            /*$tab_data = array( 
                $post->post_title,
                $post->post_content,
                $post->post_date,
                $taxonomy_names[0]->name
            );*/
            $tab_data = array();

            $tab_data[] = $post->post_date;
            $tab_data[] = $taxonomy_names[0]->name;

            $tab = explode("\n", $post->post_content);

            $tab_data[] = valueForm($tab, "Nom : ");
            $tab_data[] = valueForm($tab, "Entreprise : ");
            $tab_data[] = valueForm($tab, "Email : ");
            $tab_data[] = valueForm($tab, "Téléphone : ");
            $tab_data[] = valueFormCommentaire($tab, "Commentaire : ");
            $tab_data[] = valueForm($tab, "Créneaux disponibles : ");
            $tab_data[] = valueForm($tab, "Depuis la page : ");
            $tab_data[] = valueForm($tab, "Fichier : ");
            $tab_data[] = valueForm($tab, "Bien concerné : ");
            $tab_data[] = valueForm($tab, "URL du bien concerné : ");

            
            
            fputcsv( $output_handle, $tab_data,"\t" );
        }
        fclose( $output_handle );
        wp_reset_postdata();
		exit();
	}
}
// Instantiate a singleton of this plugin
add_action( 'init', 'wpgreen_export_contact_new');
function wpgreen_export_contact_new() {
    new wpgreen_export_contact();
}

function valueForm($tab, $var){
    foreach ($tab as $key => $value) {
        if(preg_match("#".$var."#", $value) ){
            return preg_replace( "/\r|\n/", "", str_replace($var,"", $value) );
        }                
    }
    return "";
}

function valueFormCommentaire($tab, $var){
    $r = "";
    foreach ($tab as $key => $value) {
        if(!preg_match("#Depuis la page :|Nom : |Entreprise : |Téléphone : |Créneaux disponibles : |Depuis la page : |Fichier : |Bien concerné : |Email : |URL du bien concerné : #", $value) ){
            //$r .= str_replace("Commentaire : ","", $value);
            $r .= preg_replace( "/\r|\n/", " ", str_replace("Commentaire : ","", $value) );
        }                
    }
    return $r;
}